﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public partial class MainForm : Form
    {
        Tree Tree = new Tree();
        public MainForm()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            openFileDialog.ShowDialog();
            string fileName = openFileDialog.FileName;
            string[] lines = File.ReadAllLines(fileName);
            foreach (string str in lines)
            {
                string[] words = str.Split(' ');
                foreach (string word in words)
                {
                    string tmp = word.Trim();
                    if (!string.IsNullOrEmpty(tmp))
                    {
                        Tree.Add(word);
                    }
                }
            }
            Redraw();
            openFileDialog.Dispose();
        }

        private void countWords_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Tree.CountWordsWithOddLength().ToString());
        }
        private void Redraw()
        {
            treeView1.Nodes.Clear();
            Tree.PrintToTreeView(treeView1);
            treeView1.ExpandAll();
        }

        private void delete_Click(object sender, EventArgs e)
        {
            Tree.Delete(tbDelete.Text);
            Redraw();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tree.Add(tbAdd.Text.Trim());
            Redraw();
        }
    }
}
