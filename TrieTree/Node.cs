﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TrieTree
{
    public class Node
    {
        public Dictionary<char, Node> Childs { get; set; } = new Dictionary<char, Node>();
        public bool IsWord = false;
        public int Count { get; set; }
        public void Add(string value)
        {
            if (value.Length == 0)
            {
                Count++;
                return;
            }
            if (value.Length == 1)
            {
                IsWord = true;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);

            if (!Childs.ContainsKey(next))
            {
                Node node = new Node();
                Childs.Add(next, node);
            }
            Childs[next].Add(remaining);
        }
        public bool Find(string value)
        {
            if (value.Length == 0)
            {
                return Count>0;
            }
            char next = value[0];
            string remaining = value.Length == 1 ? "" : value.Substring(1);
            if (!Childs.ContainsKey(next))
            {
                return false;
            }
            return Childs[next].Find(remaining);
        }
        public int CountWordsWithOddLength(string word, int length)
        {
            int result = 0;
            if (IsWord && length % 2 == 1)
            {
                result += 1;
            }
            foreach (var keyValuePair in Childs)
            {
                result += keyValuePair.Value.CountWordsWithOddLength(word + keyValuePair.Key, length + 1);
            }
            return result;
        } 
        public void PrintToTreeNode (TreeNode treeNode)
        {
            int i = 0;
            foreach (var keyValuePair in Childs)
            {
                treeNode.Nodes.Add(keyValuePair.Key.ToString() + (IsWord ? "." : ""));
                keyValuePair.Value.PrintToTreeNode(treeNode.Nodes[i]);
                i++;
            }
        }
        public bool Delete (string word)
        {
            if (word == "")
            {
                if (Childs.Count != 0)
                {
                    Count = 0;
                    return false;
                }
                return true;
            }
            if (word.Length == 1)
            {
                IsWord = false;
            }
            else
            {
                char next = word[0];
                string remaining = word.Length == 1 ? "" : word.Substring(1);
                if (Childs.ContainsKey(next))
                {
                    if (Childs[next].Delete(remaining))
                    {
                        Childs.Remove(next);
                        if (Childs.Count > 0)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
